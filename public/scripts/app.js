// open sidenav
function openNav() {
  document.getElementById("sidenav").style.width = "70%";
  document.getElementById("sidenav").style.height = "100vh";
}


// close sidenav
function closeNav() {
  document.getElementById("sidenav").style.width = "0";
  document.getElementById("sidenav").style.height = "100vh";
}


// close sidenav on click on header in background
document.querySelector("header").addEventListener("click", function() {
  closeNav();
});


// scroll to section on nav click
$("a[href^='#']").click(function(e) {
  e.preventDefault();

  const position = $($(this).attr("href")).offset().top;

  $("body, html").animate({
    scrollTop: position
  });
  setTimeout(() => {
    closeNav();
  }, 500);
});